<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/add_ppl/save/productList','InventoryController@addToProductList');
Route::get('/remove/list/item/','InventoryController@removeItemList');
Route::get('/edit/list/item/','InventoryController@editItemList');
Route::get('/draft/save','InventoryController@saveDraft');
Route::get('/post/document','InventoryController@postDocument');
Route::get('/add_ppl/save/productList','InventoryController@addToProductList');
Route::get('/transfer/save/document','InventoryController@saveDocument');
Route::get('/search/product','InventoryController@searchValidationItem');
Route::get('/transfer/leaving','InventoryController@deleteDocumentRecord');
Route::get('/getcheck/document','InventoryController@checkDocument');
Route::get('/update/doc/date','InventoryController@updateDocumentDate');
?>