
<section id="main-content">
	<section class="wrapper">
		<div class="container-row">
			<div class="page-header">
				<h3>{{$heading}} DOC ID: {{$doc}}</h3>
			</div>
		</div>
		<div class="container-row">
			<div class="col-md-6">
				<div class="form-group ">
				 	<label for="staticEmail" class="col-form-label">Date : </label>
				 	<input type="date" id="current_date" value="{{ date('Y-m-d') }}" class="form-control" />
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group ">
				 	<label for="staticEmail" class=" col-form-label">Platform : </label>
				 	
					<select class="form-control" id="platform">
						<option value="">All Sites</option>
						@foreach($plat as $val)
							<option value="{{$val->id}}">{{$val->platform_name}}</option>
						@endforeach
					</select>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group ">
					<div class="row">
					 	<label for="staticEmail" class="col-form-label col-md-12">SKU Code / Barcode : </label>
					 	<div id="sku_input" class="col-md-12">
					 		<input type="text" id="sku" class="form-control " />
					 	</div>
					 	<input type="button" id="btn_add_ppl" data-document="{{$doc}}" class="btn btn-info col-md-4 hidden" value="Add to Platform Product List" />
				 	</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group ">
				 	<label for="staticEmail" class="col-form-label">Quantity : </label>
				 	<input type="number" id="qty" class="form-control" disabled/>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group ">
				 	<label for="staticEmail" class="col-form-label">Description :  </label>
				 	<textarea class="form-control " disabled id="description"></textarea>
				</div>
			</div>
			<div class="col-md-6">
				<?php 
					$doc_history = App\Http\Model\DocumentModel::where('id',$doc)->where('type',$transfer_type)->first();
				?>
				<input type="text" id="page_url" class="hidden" @if($transfer_type != 0 ) value="{{url('/admin/transfer/in/'.$doc)}}" @else value="{{url('/admin/transfer/out/'.$doc)}}" @endif  />
				<input type="text" id="doc_number" class="form-control hidden" value="{{$doc}}"/>
				<input type="text" id="transfer_type" class="form-control hidden" value="{{$transfer_type}}"/>
				<input type="text" id="doc_history_status" class="form-control hidden" value="{{$doc_history['status']}}"/>
				<input type="submit" data-document="{{$doc}}" class="btn btn-warning col-md-4 col-md-offset-8" id="add-product" value="Add to Document"@if($doc_history['status'] == 1) disabled="true"  @endif /><br/>
				<input type="button" data-document="{{$doc}}" class="btn btn-info col-md-2 col-md-offset-8" id="save-product" value="Save" @if($doc_history['status'] == 1) disabled="true"  @endif />
				
				<input type="button" data-document="{{$doc}}" class="btn btn-primary col-md-2" id="post-product" value="Post" @if($doc_history['status'] == 1) disabled="true"  @endif />
			</div>
		</div>
		<div class="container-row">
			<table class="display nowrap dataTable dtr-inline collapsed" id="tbl_transferIn">
			  	<thead>
			    	<tr>
			    		<th></th>
				     	<th scope="col">Platform</th>
				      	<th scope="col">Barcode</th>
				      	<th scope="col">SKU Code</th>
				     	<th scope="col">Description</th>
				     	<th scope="col">Quantity</th>
				     	<th scope="col">Price</th>
				     	<th scope="col">Total Price</th>
			    	</tr>
			  	</thead>
			  	<tbody>
				@foreach($current_prod_history as $row)
				    <tr class="table-active">
				    	<?php 
			      			$prod_info = App\Http\Model\PlatformProductListModel::where('platform_product_list_id',$row->PPL_id)->first();
			      			$prod_desc = App\Http\Model\WatchInfoModel::where('barcode',$prod_info->barcode)->select('price','product_description')->first();
							// dd($prod_info->barcode);  
						  ?>
				    	<td>
				    		<a href="#" id="removeItem" data-content="{{$row->id}}" data-toggle="tooltip" title="Remove {{$prod_info['sku']}}" >x</a>
			    		</td>
				    	<td>
				      		<?php 
				      			$platform_info = App\Http\Model\PlatformModel::where('id',$row->platform_id)->first();
				      		?>
				      		{{$platform_info['platform_name']}}
				      	</td>
				      	<td><a href="#" id="editItem" data-content="{{$row->id}}">{{$prod_info['barcode']}}</a></td>
				      	<td><a href="#" id="editItem" data-content="{{$row->id}}">{{$prod_info['sku']}}</a></td>
				      	<td>
				      		
				      		{{str_limit($prod_desc['product_description'],30)}}
				      	</td>
				      	<td>{{$row->quantity}}</td>
				      	<td>{{number_format($prod_desc['price'],2)}}</td>
				      	<td>{{number_format($prod_desc['price'] * $row->quantity,2)}}</td>
				      	
				    </tr>
			    @endforeach
			  	</tbody>
			  	<tfoot>
		            <tr>
		            	<th></th>
		            	<th></th>
		                <th></th>
		                <th></th>
		                <th style="text-align:right">Total:</th>
		                <th></th>
		                <th></th>
		                <th></th>
		            </tr>
		        </tfoot>
			</table>
		</div>
		<div id="snackbar">
			<p id="msg_response" class="msg_response"></p>
		</div>
	</section>
	<section class="popupContents">
		<!-- The Modal -->
		<div id="myModal" class="modal">
		  	<!-- Modal content -->
		  	<div class="modal-content">
		  		<span class="close">&times;</span>
		  		<p>Update Product Quantity</p>				 
				 	<input type="text" id="history_item_id" class="form-control hidden"  />
				<div class="form-group ">
				 	<label for="history_item_quantity" class="col-form-label">Product Quantity : </label>
				 	<input type="number" id="history_item_quantity" class="form-control" />
				</div>
				<div class="form-group">
					<input type="button" class="btn btn-primary col-md-2" id="update-product-quantity" value="Update" />
				</div>				
		  	</div>
		</div>
	</section>
<style type="text/css">
	/* The Modal (background) */
	.modal {
	    display: none; /* Hidden by default */
	    position: fixed; /* Stay in place */
	    z-index: 1; /* Sit on top */
	    padding-top: 100px; /* Location of the box */
	    left: 0;
	    top: 0;
	    width: 100%; /* Full width */
	    height: 100%; /* Full height */
	    overflow: auto; /* Enable scroll if needed */
	    background-color: rgb(0,0,0); /* Fallback color */
	    background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
	}
	/* Modal Content */
	.modal-content {
	    background-color: #fefefe;
	    margin: auto;
	    padding: 20px;
	    border: 1px solid #888;
	    width: 80%;
	}
	/* The Close Button */
	.close {
	    color: #aaaaaa;
	    float: right;
	    font-size: 28px;
	    font-weight: bold;
	}
	.close:hover,
	.close:focus {
	    color: #000;
	    text-decoration: none;
	    cursor: pointer;
	}
</style>
<script>
	$(document).ready( function () {
		var numFormat = $.fn.dataTable.render.number( '\,', '.', 0, '&#8369;' ).display;
		var numFormatQty = $.fn.dataTable.render.number( '\,', '.', 0 ).display;
		// Get the modal
		var modal = document.getElementById('myModal');
		// Get the <span> element that closes the modal
		var span = document.getElementsByClassName("close")[0];
		// When the user clicks the button, open the modal 
		$('#tbl_transferIn').on('click',"a[id='editItem']",function(){
		    var id = $(this).closest(this).data('content');
		    $("#history_item_id").val(id);
		    modal.style.display = "block";
		});
		// When the user clicks on <span> (x), close the modal
		span.onclick = function() {
		    modal.style.display = "none";
		}
		// When the user clicks anywhere outside of the modal, close it
		window.onclick = function(event) {
		    if (event.target == modal) {
		        modal.style.display = "none";
		    }
		}
		$('#tbl_transferIn').DataTable({
		  	"pageLength": 200,
		  	dom: 'Brt',
		  	buttons: [
	        { 	extend: 'print', 
	        	title: '',
	        	footer: true,
	        	messageTop: function () {
                        return '{{$heading}} DOC ID: {{$doc}}';
                },
				customize: function ( win ) {
                    $(win.document.body)
                        .css( 'font-size', '10pt' )
 
                    $(win.document.body).find( 'table' )
                        .addClass( 'compact' )
                        .css( 'font-size', 'inherit' );
                }
	        }],
	        "footerCallback": function ( row, data, start, end, display ) {
	            var api = this.api(), data;
	 
	            // Remove the formatting to get integer data for summation
	            var intVal = function ( i ) {
	                return typeof i === 'string' ?
	                    i.replace(/[\$,]/g, '')*1 :
	                    typeof i === 'number' ?
	                        i : 0;
	            };
	 			
	 			// Total over all pages
	            total = api
	                .column( 5 )
	                .data()
	                .reduce( function (a, b) {
	                    return intVal(a) + intVal(b);
	                }, 0 );
	
	 
	            // Update footer
	            $( api.column( 5 ).footer() ).html(
	                numFormatQty(total) +' pcs'
	            );

	 			// Total over all pages
	            total = api
	                .column( 6 )
	                .data()
	                .reduce( function (a, b) {
	                    return intVal(a) + intVal(b);
	                }, 0 );
	
	 
	            // Update footer
	            $( api.column( 6 ).footer() ).html(
	               'P ' + numFormatQty(total) 
	            );
	 			// Total over all pages
	            total = api
	                .column( 7 )
	                .data()
	                .reduce( function (a, b) {
	                    return intVal(a) + intVal(b);
	                }, 0 );
	
	 
	            // Update footer
	            $( api.column( 7 ).footer() ).html(
	               'P ' + numFormatQty(total) 
	            );
	        }
		});
	});
	$(function(){
		$(".buttons-print").on('click',function(e){
			var doc_history_status = $("#doc_history_status").val();
			if(doc_history_status==-1){
				var popVal = confirm("Save the document first before printing");
				if(popVal == true){
					saveDocument();
					return true;
				}else{
					return false;
				}
			}
		});
		var numFormat = $.fn.dataTable.render.number( '\,', '.', 0, '&#8369;' ).display;
		var numFormatQty = $.fn.dataTable.render.number( '\,', '.', 0 ).display;
		$('#tbl_transferIn').on('click',"a[id='removeItem']",function(){
		    var id = $(this).closest(this).data('content');
			var x = document.getElementById("snackbar");
			var url      = $("#page_url").val(); 
		    x.className = "show";
		    setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
			$.get('{{url('remove')}}/list/item?id='+id,function(ret){
				$("#msg_response").html(ret.msg);
				window.location.href =  url;
			});
		});
		$("#update-product-quantity").on('click',function(e){
			var url      = $("#page_url").val(); 
			var prod_quantity = $("#history_item_quantity").val();
			var id = $("#history_item_id").val();
			var data = {
				product_quantity: $("#history_item_quantity").val(),
				history_item_id :  $("#history_item_id").val()
				};
			$.get("{{url('edit')}}/list/item",data,function(ret){
				$("#msg_response").html(ret.msg);
				wait(500);
				window.location.href =  url;
			});
		});    		
		$("#current_date").on('change',function(){
			var selectedDate = $("#current_date").val();
			var id = $("#doc_number").val();
			var trans_type = $("#transfer_type").val();
			var doc_history_status = $("#doc_history_status").val();
			var dataDate = {
				dates : selectedDate,
				id : id,
				trans_type : trans_type
			};

			var x = document.getElementById("snackbar");
			x.className = "show";
		    setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
		    if(doc_history_status==1){
		    	$("#msg_response").html("Document already posted. Date Update Failed");
		    }else{
		    	var popVal = confirm("Are you sure want to update the date?");
				if(popVal == true){
					$.get('{{url('update')}}/doc/date',dataDate,function(ret){
						$("#msg_response").html(ret.msg);
					});
					return true;
				}else{
					// $("#current_date").datepicker("setDate",<?php echo date("d-m-Y") ?>);
					$("#msg_response").html("Date update cancelled");
					return false;
				}
		    }
		});
		$("#sku").on('blur',function(e){
			CheckBarcodeSkuValidity();
		});
		$("#platform").change(function(e){
			CheckBarcodeSkuValidity();
		});
		$("#sku").keypress(function(e) {
		    if(e.which == 13) {
				e.preventDefault();
		        // SaveToProductList()
		        CheckBarcodeSkuValidity();
		    }
		});
		$("#qty").keypress(function(e) {
		    if(e.which == 13) {
				e.preventDefault();
		        SaveToProductList();
		    }
		});
		$("#add-product").click(function(e){
			e.preventDefault();
			SaveToProductList();
		});
		$("#save-product").click(function(e){
			saveDocument();
		});
		$("#post-product").click(function(e){
			 var popVal = confirm("Posting Document cannot be edited anymore. If there are any problems please contact Operations Manager");
			 if(popVal==true){
				postDocument();	
			 }else{
			 	return false;
			 }
			
		});
		$("#qty").keyup(function(e){
			var qtyVal = $("#qty").val();
			if(qtyVal < 0){
				var x = document.getElementById("snackbar");
			    x.className = "show";
			    setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
			    $("#msg_response").html('Quantity Cannot be less than 0');
			    $("#add-product").prop('disabled','true');
			}else{
				$("#add-product").removeAttr('disabled');
			}
		});
		$("#btn_add_ppl").click(function(e){
			addToProducts();
		});
		function wait(ms){
		   var start = new Date().getTime();
		   var end = start;
		   while(end < start + ms) {
		     end = new Date().getTime();
		  }
		};
		function saveDocument(){
			var doc_id = $("#doc_number").val();
			var transfer_type = $("#transfer_type").val();
			var x = document.getElementById("snackbar");
		    x.className = "show";
		    setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
			$.get('{{url('draft')}}/save?doc_id='+doc_id+'&trans_type='+transfer_type, function(data){
				if(data.status == 1){
				    $("#msg_response").html(data.msg);
				    $("#save-product").prop('disabled','true');
				}else{
				    $("#msg_response").html(data.msg);
				}
			});
		}
		function postDocument(){
			var doc_id = $("#doc_number").val();
			var transfer_type = $("#transfer_type").val();
			var x = document.getElementById("snackbar");
		    x.className = "show";
		    setTimeout(function(){ x.className = x.className.replace("show", ""); }, 4000);
			$.get('{{url('post')}}/document?doc_id='+doc_id+'&trans_type='+transfer_type,function(data){
				if(data.status == 1){
				    $("#msg_response").html(data.msg);
				    $("#post-product").prop('disabled','true');
				    $("#save-product").prop('disabled','true');
				}else{
				    $("#msg_response").html(data.msg);
				}
				console.arr;
			});
		};
		function addToProducts(){
			var sku = $("#sku").val();
			var doc_id = $("#doc_number").val();
			var platform = $("#platform").val();
			var transfer_type = $("#transfer_type").val();
			var x = document.getElementById("snackbar");
		    x.className = "show";
		    setTimeout(function(){ x.className = x.className.replace("show", ""); }, 4000);
			
			$.get('{{url('add_ppl')}}/save/productList?sku=' + encodeURIComponent(sku) + '&document_id=' + doc_id + '&platform=' + platform+'&trans_type='+transfer_type,function(data){
				if(data.status == 1){
					$("#btn_add_ppl").addClass("hidden");
				    $("#sku_input").addClass("col-md-12").removeClass("col-md-8");
				    $("#msg_response").html(data.msg);
				    $('#qty').removeAttr('disabled');
				    $("#qty").focus();
				    $('#save-product').removeAttr('disabled');
					return false;
				}else{
					$("#msg_response").html(data.msg);
					$('#qty').prop('disabled','true');
				}
			});
			
		};
		function SaveToProductList(){
			var docs = $("#doc_number").val();
			var	date 	 = $("#current_date").val();
			var	platform = $("#platform").val();
			var	sku 	 = $("#sku").val();
			var	quantity = $("#qty").val();
			var transfer_type = $("#transfer_type").val();
			console.log(sku);

			var x = document.getElementById("snackbar");
		    x.className = "show";
		    setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
			if(date==""){
			    $("#msg_response").html('Date cannot be empty!');
				return false;
			}
			if(platform==""){
			    $("#msg_response").html('platform cannot be empty!');
				return false;
			}
			if(quantity==""){
			    $("#msg_response").html('Quantity cannot be empty!');
				return false;
			}
			if(sku==""){
			    $("#msg_response").html('SKU or Barcode cannot be empty!');
				return false;
			}
			$.get('{{url('getcheck')}}/document?doc_id=' + docs + '&trans_type='+transfer_type,function(ret){
				
				switch(ret.status){
					case 1:
						alert('Document cannot be modified');
						break;
					case -1:
					case 0 :
						$.get('{{url('transfer')}}/save/document?date=' + date + '&plat=' + platform + '&sku=' + encodeURIComponent(sku) + '&qty=' + quantity + '&doc_id=' + docs+'&trans_type='+transfer_type,  function(data){
							if(data.status == 1){
								$("#tbl_transferIn > tfoot > tr:last").remove();
							    $("#msg_response").html(data.msg);
							    $('#tbl_transferIn > tbody:first').append(
							    	'<tr><td><a href="#" id="removeItem" data-content="'+ data.history_id +'"> x</a> </td><td>' 
							    	+ data.platform+ '</td><td><a href="#" id="editItem" data-content="'+ data.history_id +'">' 
							    	+ data.barcode + '</a></td><td><a href="#" id="editItem" data-content="'+ data.history_id +'">' 
							    	+ data.sku+ '</a></td><td>' 
							    	+ data.desc + '</a></td><td>' 
							    	+ data.qty + '</a></td><td>' 
							    	+ data.price + '</a></td><td>' 
							    	+ data.totalPrice+ '</td></tr>'); 
							    $("#qty").val("");
							    $("#sku").val("");
							    $("#sku").focus();
							    
							    $("#tbl_transferIn > tfoot:first").append(
							    		'<tr><th></th><th></th><th></th><th></th><th style="text-align:right">Total:</th><th>'
							    		+ numFormatQty(data.finalQuantity) + '</th><th>'
							    		+ numFormat(data.finalPrice) +'</th><th>'
							    		+ numFormat(data.finalTotalPrice) + '</th></tr>'
							    		);

								return true;
							}else{
							    $("#msg_response").html(data.msg);
								return true;
							}
						});
						break;
					default:
						alert('PLease Contact Administrator to check the problem');
						break;
				}
			});
		}

		function CheckBarcodeSkuValidity(){
			var platform = $("#platform").val();
			var sku = $("#sku").val();
			var x = document.getElementById("snackbar");
				    x.className = "show";
				    setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
			if(platform==""){
				
			    $("#msg_response").html('Please select platform!');
				return false;
			}
			if(sku==""){
				
			    $("#msg_response").html('Barcode / SKU cannot be empty');
				return false;
			}
			$.get('{{url('search')}}/product?sku=' + encodeURIComponent(sku) + '&platform=' + platform,function(data){
				if(data.status == '0'){
					// $("#btn_add_ppl").removeClass("hidden");
				    // $("#sku_input").removeClass("col-md-12").addClass("col-md-8");
				    $("#msg_response").html(data.msg);
				    $("#description").val(data.desc);
				    $('#qty').prop('disabled','true');
				    console.log("Not Found");
				     addToProducts();
					return false;
				}else{
					$("#btn_add_ppl").addClass("hidden");
				    $("#sku_input").addClass("col-md-12").removeClass("col-md-8");
				    $("#msg_response").html(data.msg);
				    $("#description").val(data.desc);
				    $('#qty').removeAttr('disabled');
				    $('#qty').focus();
				    console.log("Found");
					return false;
				}
			});
		};
		function leavingPage(){
			var popVal = confirm("Please don't leave me!");
		    console.log(popVal);
		    if(popVal == true){
				var doc_id =  $("#doc_number").val();
				var transfer_type = $("#transfer_type").val();
		    	$.get('{{url('transfer')}}/leaving?doc_id='+doc_id+'&trans_type='+$transfer_type,function(data){
		    		console(data.msg);
		    		return true;
	    		});
		    }else{
		    	return false;
		    }
		}
		var skupath = "{{url('/sku/search/autocomplete')}}";
	  	$('#sku').typeahead({
	  		source:function(query,process){
	  			return $.get(skupath, {query:query},function(data){
	  				return process(data);
	  			});
	  		}
	  	});
	});
</script>